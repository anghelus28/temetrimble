import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'project';
  titleText:string="Test";
  dateTest:Date= new Date(6,28,2002);
  myValue:number=10;
}
