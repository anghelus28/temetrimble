import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Comp1Component } from './comp1/comp1.component';
import { M1Module } from './m1/m1.module';



@NgModule({
  declarations: [
    Comp1Component
  ],
  imports: [
    CommonModule,
    M1Module
  ]
})
export class Ex2Module { }
