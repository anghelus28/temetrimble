import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { C2Component } from './c2/c2.component';
import { M2Module } from './m2/m2.module';



@NgModule({
  declarations: [
    C2Component
  ],
  imports: [
    CommonModule,
    M2Module
  ]
})
export class M1Module { }
