import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { C3Component } from './c3/c3.component';
import { M3Module } from './m3/m3.module';



@NgModule({
  declarations: [
    C3Component
  ],
  imports: [
    CommonModule,
    M3Module
  ]
})
export class M2Module { }
