import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { C4Component } from './c4/c4.component';



@NgModule({
  declarations: [
    C4Component
  ],
  imports: [
    CommonModule
  ]
})
export class M3Module { }
