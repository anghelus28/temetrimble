import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tema3',
  templateUrl: './tema3.component.html',
  styleUrls: ['./tema3.component.scss']
})
export class Tema3Component implements OnInit {

  text:string="Tema3";
  textBackground:string ="";
  nContent:string= "";
  
  constructor() { }

  ngOnInit(): void {
  }
  
  setColor() { this.textBackground=this.nContent; }

}
