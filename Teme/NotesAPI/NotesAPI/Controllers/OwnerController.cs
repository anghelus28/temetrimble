﻿using Microsoft.AspNetCore.Mvc;
using NotesAPI.Models;
using NotesAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class OwnersController : ControllerBase
    {
        IOwnerCollectionService _ownerCollectionService;
        public OwnersController(IOwnerCollectionService ownerCollectionService)
        {
            _ownerCollectionService = ownerCollectionService ?? throw new ArgumentNullException(nameof(ownerCollectionService));
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _ownerCollectionService.GetAll());
        }

        [HttpPost]
        public async Task<IActionResult> CreateOwner([FromBody] Owner owner)
        {
            if (owner == null)
                return BadRequest("Owner is null");
            await _ownerCollectionService.Create(owner);
            return Ok(await _ownerCollectionService.GetAll());
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOwner(Guid id)
        {
            await _ownerCollectionService.Delete(id);
            return Ok(await _ownerCollectionService.GetAll());
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateOwner(Guid id, [FromBody] Owner owner)
        {
            await _ownerCollectionService.Update(id, owner);
            return Ok(await _ownerCollectionService.GetAll());
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> GetOwner(Guid id)
        {
            await _ownerCollectionService.Get(id);
            return Ok(await _ownerCollectionService.GetAll());
        }
    }
}
