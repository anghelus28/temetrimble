﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        static List<Category> categories = new List<Category> {
        new Category{Name="To Do", Id="1"},
        new Category{ Name= "Done", Id= "2"},
        new Category{ Name= "Doing", Id= "3"}
        };

        /// <summary>
        /// Get the whole collection of categories
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(categories);
        }

        /// <summary>
        /// Get the element with the given id
        /// </summary>
        [HttpGet("{id}", Name = "GetCategory")]
        public IActionResult GetCategory(string id)
        {
            return Ok(categories.Where((category) => category.Id == id));
        }

        /// <summary>
        /// Create a new category and add it to the category list
        /// </summary>
        [HttpPost]
        public IActionResult CreateCategory([FromBody] Category category)
        {
            categories.Add(category);
            return CreatedAtRoute("GetCategory", new { id = category.Id }, category);
        }

        /// <summary>
        /// Delete the category with the given id  
        /// </summary>
        [HttpDelete("{id}")]
        public IActionResult DeleteCategory(string id)
        {
            categories.RemoveAt(categories.FindIndex((category) => category.Id == id));
            return Ok(categories);
        }
    }
}
