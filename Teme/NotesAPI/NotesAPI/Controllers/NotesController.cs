﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using NotesAPI.Models;
using System.Threading.Tasks;
using NotesAPI.Services;

namespace NotesAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class NotesController : ControllerBase
    {
        INoteCollectionService _noteCollectionService;
        public NotesController(INoteCollectionService noteCollectionService)
        {
            _noteCollectionService = noteCollectionService ?? throw new ArgumentNullException(nameof(noteCollectionService));
        }

        [HttpGet]

        public async Task<IActionResult> GetNotes()
        {
            return Ok(await _noteCollectionService.GetAll());
        }

        [HttpPost]

        public async Task<IActionResult> CreateNote([FromBody] Note note)
        {
            if (note.Id == Guid.Empty)
                note.Id = Guid.NewGuid();
            if (note == null)
                return BadRequest("Note is null");
            await _noteCollectionService.Create(note);
            return Ok(await _noteCollectionService.GetAll());
        }

        [HttpPut("{id}")]

        public async Task<IActionResult> UpdateNote(Guid id, [FromBody] Note note)
        {
            await _noteCollectionService.Update(id, note);
            return Ok(await _noteCollectionService.GetAll());
        }

       [HttpDelete("{id}")]

        public async Task<IActionResult> DeleteNote(Guid id)
        {
            await _noteCollectionService.Delete(id);
            return Ok(await _noteCollectionService.GetAll());
        }

        [HttpGet("{id}")]

        public async Task<IActionResult> GetNotesById(Guid id)
        {
            await _noteCollectionService.Get(id);
            return Ok(await _noteCollectionService.GetAll());
        }

        [HttpGet("owner/{id}")]

        public async Task<IActionResult> GetNotesByOwnerId(Guid id)
        {
            await _noteCollectionService.GetNotesByOwnerId(id);
            return Ok(await _noteCollectionService.GetAll());
        }
    }

}