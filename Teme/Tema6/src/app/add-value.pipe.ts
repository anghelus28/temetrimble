import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'addValue'
})
export class AddValuePipe implements PipeTransform {

  transform(value: number, AddValue: number): number {
    return isNaN(AddValue)? value : value + AddValue;
  }

}
