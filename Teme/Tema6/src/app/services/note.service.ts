import { Injectable } from '@angular/core';
import { Note } from '../note';

@Injectable({
  providedIn: 'root'
})
export class NoteService {

  _id:number=5;

  notes: Note[] = [
    {
      id: "Id1",
      title: "First note",
      description: "This is the description for the first note",
      categoryId:"1"
    },
    {
      id: "Id2",
      title: "Second note",
      description: "This is the description for the second note",
      categoryId:"2"},
    {
      id: "Id3",
      title: "Third note",
      description: "This is the description for the third note",
      categoryId:"3"
    },
    {
      id: "Id4",
      title: "Third note",
      description: "This is the description for the third note",
      categoryId:"1"
    }
  ];
  
  constructor() { }
  serviceCall() {
    console.log("Note service was called");
  }
  getNotes() {
    return this.notes;
  }
  
  insertNote(_title:string,_description:string){
    this._id++;
    this.notes.push({
      id: "Id"+this._id.toString(),
      title: _title,
      description: _description,
      categoryId:"1"
    })
  }
  
  getFilteredNotes(argCategoryId:string){
    return this.notes.filter((nota=>nota.categoryId==argCategoryId));
  }
}

