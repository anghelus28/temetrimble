import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-tema4',
  templateUrl: './tema4.component.html',
  styleUrls: ['./tema4.component.scss']
})
export class Tema4Component implements OnInit {

  description:string;

  constructor(private _router:Router,private _activatedRoute:ActivatedRoute) { }

  ngOnInit(): void {
    this._activatedRoute.params.subscribe(parameter=>{
      console.log(parameter)
    })
  }

}
