import { Component, OnInit } from '@angular/core';
import { NoteService } from '../services/note.service';

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.scss'],
})
export class AddNoteComponent implements OnInit {

  title:string;
  description:string;

  constructor(private service:NoteService) { }

  ngOnInit(): void {
  }
  add(){
     this.service.insertNote(this.title,this.description);
  }
}
