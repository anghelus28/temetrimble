import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pipeTema'
})
export class PipeTemaPipe implements PipeTransform {

  transform( myString: string): string {
    return "*"+myString+"*";
  }

}
