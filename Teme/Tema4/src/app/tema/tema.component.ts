import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tema',
  templateUrl: './tema.component.html',
  styleUrls: ['./tema.component.scss']
})
export class TemaComponent implements OnInit {

  vector:string[]=[
    "Acesta",
    "nu",
    "este",
    "nu",
    "un",
    "titlu",
    "text"
  ]

  dates:Date[]=[
    new Date("2022-03-08"),
    new Date("2021-06-28"),
    new Date("2020-01-05"),
    new Date("2023-04-12"),
    new Date("2022-09-17")
  ]

  constructor() { }

  ngOnInit(): void {
  }
}
