import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HighlightDirective } from './highlight.directive';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TemaComponent } from './tema/tema.component';
import { PipeTemaPipe } from './pipe-tema.pipe';

@NgModule({
  declarations: [
    AppComponent,
    TemaComponent,
    PipeTemaPipe,
    HighlightDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
