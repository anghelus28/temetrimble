import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  categoryId:string;

  constructor() { }
  receiveCategory(categoryId:string){
this.categoryId=categoryId;
console.log(this.categoryId);
}
  ngOnInit(): void {
  }

}
