import { Component, OnInit } from '@angular/core';
import { Category } from '../category';
import { Note } from '../note';
import { NoteService } from '../services/note.service';


@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.scss'],
})

export class AddNoteComponent implements OnInit {
  
  categories:Category[]=[
    {name:'All',id:'0',a:'0'},
    {name:'To Do',id:'1',a:'0'},
    {name:'Done',id:'2',a:'0'},
    {name:'Doing',id:'3',a:'0'}
  ]
  title:string;
  description:string;
  selectedCategory:string;

  constructor(private service:NoteService) { }

  ngOnInit(): void {
  }
  public add(){
    const note:Note={
      title:this.title,
      description:this.description,
      categoryId:this.selectedCategory
    }
    console.log(this.selectedCategory);
    this.service.addNote(this.title,this.description,this.selectedCategory);
     //this.service.insertNote(_title,_description);
  }

}
