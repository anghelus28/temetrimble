import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Category } from '../category';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  @Output() emitSelectedFilter = new EventEmitter<string>();

  categories:Category[]=[
    {name:'All',id:'0',a:'0'},
    {name:'To Do',id:'1',a:'0'},
    {name:'Done',id:'2',a:'0'},
    {name:'Doing',id:'3',a:'0'}
  ]

  constructor() { }
  selectFilter(categoryId: string) {
    this.emitSelectedFilter.emit(categoryId);
  }
  ngOnInit(): void {
  }

}
