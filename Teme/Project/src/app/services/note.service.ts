import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Note } from '../note';

@Injectable({
  providedIn: 'root'
})
export class NoteService {

  _id:number=5;

  readonly baseUrl= 'https://localhost:4200';

  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  
  constructor(private httpClient: HttpClient) { }
  
  getNotes():Observable<Note[]> {
    return this.httpClient.get<Note[]>(this.baseUrl+'/notes', this.httpOptions);
}

getFiltredNotes(categId:string){
    return this.httpClient.get<Note[]>(this.baseUrl+'/notes', this.httpOptions).pipe(map((notes:Note[])=>{
      return notes.filter((note)=>note.categoryId==categId)
  }));
}
addNote(noteTitle:string, noteDescription:string,noteCategory:string){

  let note= {  
              description: noteDescription,
              title: noteTitle,
              categoryId:noteCategory
              }
  return  this.httpClient.post(this.baseUrl+"/notes", note, this.httpOptions).subscribe();
            }
}

